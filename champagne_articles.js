const clientId = Deno.env.get('CLIENT_ID')
const clientIntegrationId = Deno.env.get('CLIENT_INTEGRATION_ID')
const integratorApiUrl = Deno.env.get("INTEGRATOR_API")
const clientEcommerce = JSON.parse(Deno.env.get('CLIENT_ECOMMERCE'))

const champagneUrl = "http://champagnehome.com.uy/api/products.json"

const removeTags = (str) => {
    if ((str === null) || (str === '')) {
        return str;

    }
    return str.replace(/(<([^>]+)>)/ig, '');
}

async function getExistingArticles() {
    let articleSkus = [];
    for (let ecommerceId of Object.values(clientEcommerce)) {
      let response = await fetch(
        `${integratorApiUrl}/api/client/${clientId}/ecommerce/${ecommerceId}/products`
      );
      let allArticleSkusAsKeys = await response.json();
  
      for (let articleSku of Object.keys(allArticleSkusAsKeys)) {
        if (!articleSkus.find((x) => x == articleSku)) {
          articleSkus.push(articleSku);
        }
      }
    }
  
    return articleSkus;
}

async function createExistingArticlesMap(existingArticles) {
    let existingArticlesMap = {};
    for(let article of existingArticles) {
        existingArticlesMap[article] = true;
    }
    
    return existingArticlesMap;
}

async function fetchWithTimeout(url, options = {}) {
    const { timeout = 8000 } = options;
    
    const controller = new AbortController();
    const id = setTimeout(() => controller.abort(), timeout);
    const response = await fetch(url, {
      ...options,
      signal: controller.signal  
    });
    clearTimeout(id);
    return response;
  }

const init = async () => {
    try {
        console.log("Champagne article fetch started!")
        
        let existingArticles = await getExistingArticles();
        console.log(`Existing articles retrieved: ${existingArticles.length} found.`);
        let existingArticlesMap = await createExistingArticlesMap(existingArticles);
        let deactivate = false;
        const response = await fetchWithTimeout(champagneUrl, {timeout: 4000000});
        let data = await response.json();
        data = Object.values(data)
        console.log(`Champagne articles fetched! ${data.length} found.`)
        if(data.length >= (existingArticles.length * 0.4) && data.length >= 0) {
            deactivate = true;
        }
        for (let element of data) {
            if (element && element.SKU != '') {
                let product = {
                    sku: element.SKU,
                    client_id: clientId,
                    options: {
                        merge: false,
                    },
                    integration_id: clientIntegrationId,
                    ecommerce: Object.values(clientEcommerce).map(ecommerce_id => {
                        return {
                            ecommerce_id: ecommerce_id,
                            variants: [],
                            properties: [
                                { "name": element.title },
                                { "description": removeTags(element.description) },
                                { "stock": element.stock },
                                { "images": element.images ? element.images.map(x => x.url) : [] },
                                {
                                    "price": {
                                        "value": element.price_special ? element.price_special : element.price,
                                        "currency": element.currency,
                                    },
                                }
                            ]
                        }
                    })

                }
                sagalDispatch(product)
                if(deactivate && existingArticlesMap[element.SKU]) {
                    existingArticlesMap[element.SKU] = null
                }
            }
        }
        if(deactivate) {
            for(let [existingArticle, markedAsTrue] of Object.entries(existingArticlesMap)) {
                let productToDeactivate = {
                    sku: existingArticle,
                    client_id: clientId,
                    options: {
                        merge: false,
                    },
                    integration_id: clientIntegrationId,
                    ecommerce: Object.values(clientEcommerce).map(ecommerce_id => {
                        return {
                            ecommerce_id: ecommerce_id,
                            variants: [],
                            properties: [
                                { "stock": 0 }
                            ]
                        }
                    })
                }
                if(markedAsTrue) {
                    console.log(`Deactivating ${existingArticle}...`)
                    sagalDispatch(productToDeactivate)
                }
            }
        }
    } catch (e) {
        `Failed to fetch files from Corbeta, error ${e.message}`
        throw e
    }
}
await init();
